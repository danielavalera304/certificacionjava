# README #



###  EL PROYECTO GRUPAL DE DAM 2016/17 ###

* recuerda que no participar correctamente en el proyecto restará puntos

### CÓMO CLONARLO CORRECTAMENTE ###

*  Recuerda que al clonar el proyecto debes adaptarlo a tu entorno local, 
y luego ignorar los cambios en el archivo del proyecto

### Contribution guidelines ###

* Cada objetivo está en una carpeta distinta
* Sólo debes realizar cambios en la carpeta de tu parte del proyecto

### CADA EQUIPO TIENE UN RESPONSABLE ###

* lo ideal es que los cambios al proyecto total los haga sólo el responsable del grupo